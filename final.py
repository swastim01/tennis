class Player:
    def __init__(self, name):
        self.name = name
        self.points = 0
        self.games = 0
        self.score = 0

    def win_point(self):
        self.points += 1

    def win_game(self):
        self.games += 1
        self.points = 0

    def win_set(self):
        self.score += 1
        self.games = 0

    def get_display_points(self):
        if self.points == 0:
            return "0"
        elif self.points == 1:
            return "15"
        elif self.points == 2:
            return "30"
        elif self.points == 3:
            return "40"
        else:
            return "Ad"

    def __str__(self):
        return f'{self.name}: Points: {self.get_display_points()}, Games: {self.games}, Score: {self.score}'

class TennisMatch:
    def __init__(self, player_a_name, player_b_name):
        self.player_a = Player(player_a_name)
        self.player_b = Player(player_b_name)
    
    def play_point(self, winner):
        if winner == 'A':
            self.player_a.win_point()
        elif winner == 'B':
            self.player_b.win_point()
        
        self.check_game_winner()

    def check_game_winner(self):
        if self.player_a.points >= 4 and (self.player_a.points - self.player_b.points) >= 2:
            self.player_a.win_game()
        elif self.player_b.points >= 4 and (self.player_b.points - self.player_a.points) >= 2:
            self.player_b.win_game()
        
        self.check_set_winner()

    def check_set_winner(self):
        if self.player_a.games >= 6 and (self.player_a.games - self.player_b.games) >= 2:
            self.player_a.win_set()
        elif self.player_b.games >= 6 and (self.player_b.games - self.player_a.games) >= 2:
            self.player_b.win_set()

    def play_match(self, points_sequence):
        for point in points_sequence:
            self.play_point(point)
        self.display_score()

    def display_score(self):
        print(self.player_a)
        print(self.player_b)

def scoreboard():
    match = TennisMatch("Player A", "Player B")
    points_sequence = "ABBBBAABB"
    match.play_match(points_sequence)


scoreboard()
